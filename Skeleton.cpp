//=============================================================================================
// Mintaprogram: Z?ld h?romsz?g. Ervenyes 2019. osztol.
//
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat, BOM kihuzando.
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni a printf-et kiveve
// - Mashonnan atvett programresszleteket forrasmegjeloles nelkul felhasznalni es
// - felesleges programsorokat a beadott programban hagyni!!!!!!!
// - felesleges kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan OpenGL fuggvenyek hasznalhatok, amelyek az oran a feladatkiadasig elhangzottak
// A keretben nem szereplo GLUT fuggvenyek tiltottak.
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : Szommer Zsombor
// Neptun : MM5NOT
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem.
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem,
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================
#include "framework.h"

const char *vertexSource0 = R"(
	#version 330
	precision highp float;

	layout(location = 0) in vec3 vertexPosition;
	layout(location = 1) in vec3 vertexColor;

	out vec3 color;

	void main()
	{
		color = vertexColor;
		gl_Position = vec4(vertexPosition.x, vertexPosition.y, 0, vertexPosition.z);
	}
)";

const char *fragmentSource0 = R"(
	#version 330
	precision highp float;

	in vec3 color;
	out vec4 fragmentColor;

	void main()
	{
		fragmentColor = vec4(color, 1);
	}
)";

const char *vertexSource1 = R"(
	#version 330
	precision highp float;

	layout(location = 0) in vec3 vertexPosition;
	layout(location = 1) in vec2 vertexUV;

	out vec2 texCoord;

	void main()
	{
		texCoord = vertexUV;
		gl_Position = vec4(vertexPosition.x, vertexPosition.y, 0, vertexPosition.z);
	}
)";

const char *fragmentSource1 = R"(
	#version 330
	precision highp float;

	uniform sampler2D textureUnit;

	in vec2 texCoord;
	out vec4 fragmentColor;

	void main()
	{
		fragmentColor = texture(textureUnit, texCoord);
	}
)";

GPUProgram gpuPrograms[2];

vec3 middle = vec3(0, 0, 1);
vec3 previousMousePosition;
bool mouseButtonDown = false;
bool physicsEnabled = false;

long previousTime;
float deltaTime;

const float reallySmallFloat = 0.0001f;
const float preferredDistance = 1.0f;

const int circleSides = 4;

inline vec3 &operator+=(vec3 &v1, const vec3 &v2)
{
	return v1 = v1 + v2;
}

inline bool isNan(const vec3 &vec)
{
	return (isnan(vec.x) || isnan(vec.y) || isnan(vec.z));
}

inline float Lorentz(const vec3 &p, const vec3 &q)
{
	return p.x * q.x + p.y * q.y - p.z * q.z;
}

inline float distance(const vec3 &p, const vec3 &q)
{
	float lorentz = Lorentz(p, q);
	return fabs(-lorentz - 1) < reallySmallFloat ? 0 : acoshf(-lorentz);
}

inline vec3 corrigate(const vec3 &vec)
{
	return vec * sqrtf(-1 / Lorentz(vec, vec));
}

vec3 moveWithVelocity(const vec3 &pointToMove, const vec3 &velocity, const float &time)
{
	vec3 retval;
	float speed = length(velocity);
	if (speed < reallySmallFloat)
	{
		retval = pointToMove;
	}
	else
	{
		vec3 direction = velocity / speed;
		retval = pointToMove * coshf(speed * time) + direction * sinhf(speed * time);
	}
	return corrigate(retval);
}

vec3 unitVectorFromPoints(const vec3 &p, const vec3 &q)
{
	float d = distance(p, q);
	return d < reallySmallFloat ? vec3(0, 0, 0) : (q - p * cosh(d)) / sinh(d);
}

vec3 mirror(vec3 pointToMirror, vec3 mirrorPoint)
{
	vec3 retval;
	float d = distance(pointToMirror, mirrorPoint);
	if (d < reallySmallFloat)
	{
		retval = pointToMirror;
	}
	else
	{
		vec3 v = unitVectorFromPoints(pointToMirror, mirrorPoint);
		retval = pointToMirror * coshf(2 * d) + v * sinhf(2 * d);
	}
	return corrigate(retval);
}

vec3 midpoint(const vec3 &p, const vec3 &q)
{
	vec3 retval;
	float d = distance(p, q);
	if (d < reallySmallFloat)
	{
		retval = p;
	}
	else
	{
		vec3 v = unitVectorFromPoints(p, q);
		retval = p * coshf(0.5 * d) + v * sinhf(0.5 * d);
	}
	return corrigate(retval);
}

vec3 translatePoint(const vec3 &pointToTranslate, const vec3 &p, const vec3 &q)
{
	vec3 m1 = p;
	vec3 m2 = midpoint(p, q);
	vec3 retval = mirror(mirror(pointToTranslate, m1), m2);
	return corrigate(retval);
}

inline float edgeForce(const float &distance)
{
	return powf((distance - preferredDistance), 3) * 16.0f;
}

inline float notEdgeForce(const float &distance)
{
	float force = powf((distance - preferredDistance), 3) * 4.0f;
	return force < 0 ? force : 0;
}

inline float globalForce(const float &distance)
{
	return 1.0f;
}

vec4 randomColor()
{
	float red = (float)(rand() % 256) / 256;
	float green = (float)(rand() % 256) / 256;
	float blue = (float)(rand() % 256) / 256;
	return vec4(red, green, blue, 1.0f);
}

class Node
{
private:
	vec3 coordinate;
	vec3 velocity;
	float mass;
	std::vector<Node *> adjacentNodes;
	std::vector<Node *> notAdjacentNodes;

	vec3 circle[circleSides];
	vec2 uvs[circleSides];
	Texture *texture;

	unsigned int vao;
	unsigned int vbo[2];

	void generateTexture()
	{
		std::vector<vec4> image;

		int random = rand() % 3;

		vec4 a;
		vec4 b;

		switch (random)
		{
		case 0:
			a = randomColor();
			b = randomColor();
			image.push_back(a);
			image.push_back(a);
			image.push_back(b);
			image.push_back(b);
			break;
		case 1:
			a = randomColor();
			b = randomColor();
			image.push_back(a);
			image.push_back(b);
			image.push_back(a);
			image.push_back(b);
			break;
		case 2:
			image.push_back(randomColor());
			image.push_back(randomColor());
			image.push_back(randomColor());
			image.push_back(randomColor());
			break;
		default:
			break;
		}
		this->texture = new Texture(2, 2, image, GL_NEAREST);
	}

public:
	Node(vec3 coordinate)
	{
		this->coordinate = coordinate;
		this->velocity = vec3(0, 0, 0);
		this->mass = 1;

		this->generateTexture();

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(2, &vbo[0]);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		this->updateCircle();
		glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * circleSides, this->circle, GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
		for (int i = 0; i < circleSides; ++i)
		{
			float fi = i * 2 * M_PI / circleSides;
			float x = cos(fi + M_PI / 4) * 0.5 + 0.5;
			float y = sin(fi + M_PI / 4) * 0.5 + 0.5;
			this->uvs[i] = vec2(x, y);
		}
		glBufferData(GL_ARRAY_BUFFER, circleSides * sizeof(vec2), this->uvs, GL_STATIC_DRAW);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	}

	void draw()
	{
		gpuPrograms[1].setUniform(*(this->texture), "textureUnit");
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * circleSides, this->circle, GL_DYNAMIC_DRAW);
		glDrawArrays(GL_TRIANGLE_FAN, 0, circleSides);
	}

	void updateCircle()
	{
		for (int i = 0; i < circleSides; ++i)
		{
			float scale = 0.05f;
			float fi = i * 2 * M_PI / circleSides;
			float x = cos(fi + M_PI / 4) * scale + this->coordinate.x;
			float y = sin(fi + M_PI / 4) * scale + this->coordinate.y;
			float w = sqrtf(x * x + y * y + 1);
			circle[i] = vec3(x, y, w);
		}
	}

	const vec3 &getCoordinate()
	{
		return this->coordinate;
	}

	void addAdjacentNode(Node *adjacentNode)
	{
		this->adjacentNodes.push_back(adjacentNode);
	}

	void addNotAdjacentNode(Node *notAdjacentNode)
	{
		this->notAdjacentNodes.push_back(notAdjacentNode);
	}

	void changeToAdjacentNode(Node *node)
	{
		bool found = false;
		for (int i = 0; i < (int)this->notAdjacentNodes.size() && !found; ++i)
		{
			if (this->notAdjacentNodes[i] == node)
			{
				this->notAdjacentNodes.erase(this->notAdjacentNodes.begin() + i);
				found = true;
			}
		}
		if (found)
		{
			this->adjacentNodes.push_back(node);
		}
	}

	void letItmove()
	{
		vec3 edgeForceSum = vec3(0, 0, 0);
		for (auto node : this->adjacentNodes)
		{
			vec3 unitVector = unitVectorFromPoints(this->coordinate, node->coordinate);
			float force = edgeForce(distance(this->coordinate, node->coordinate));
			vec3 product = unitVector * force;
			edgeForceSum += product;
		}

		vec3 notEdgeForceSum = vec3(0, 0, 0);
		for (auto node : this->notAdjacentNodes)
		{
			vec3 unitVector = unitVectorFromPoints(this->coordinate, node->coordinate);
			float force = notEdgeForce(distance(this->coordinate, node->coordinate));
			vec3 product = unitVector * force;
			notEdgeForceSum += product;
		}

		vec3 global = unitVectorFromPoints(this->coordinate, middle) * globalForce(distance(this->coordinate, middle));

		vec3 resistanceForce = 0.01f * this->velocity;

		vec3 summedForce = edgeForceSum + notEdgeForceSum + global - resistanceForce;

		this->velocity += summedForce / this->mass * deltaTime;

		vec3 p2 = moveWithVelocity(this->coordinate, this->velocity, deltaTime * 2);

		this->coordinate = moveWithVelocity(this->coordinate, this->velocity, deltaTime);
		this->updateCircle();

		this->velocity = length(this->velocity) * unitVectorFromPoints(this->coordinate, p2);
	}

	void translate(const vec3 &m1, const vec3 &m2)
	{
		this->coordinate = translatePoint(this->coordinate, m1, m2);
		this->updateCircle();
	}

	void applyHeuristics()
	{
		vec3 newCoordinate = this->coordinate;
		float totalMass = 0;
		for (auto adjacentNode : this->adjacentNodes)
		{
			vec3 v = unitVectorFromPoints(newCoordinate, adjacentNode->coordinate);
			float d = distance(newCoordinate, adjacentNode->coordinate);
			float m = adjacentNode->mass;
			newCoordinate = moveWithVelocity(newCoordinate, v, d * m / (totalMass + m));
			totalMass += m;
			newCoordinate = corrigate(newCoordinate);
		}
		this->coordinate = newCoordinate;
		this->updateCircle();
	}

	~Node()
	{
		delete this->texture;
	}
};

class Edge
{
private:
	Node *node0, *node1;

	vec3 vertexCoords[2];

	unsigned int vao;
	unsigned int vbo[2];

public:
	Edge(Node *node0, Node *node1)
	{
		this->node0 = node0;
		this->node1 = node1;

		node0->changeToAdjacentNode(node1);
		node1->changeToAdjacentNode(node0);

		this->updateVertexCoords();

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(2, &vbo[0]);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(vec3), this->vertexCoords, GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
		vec3 colors[2] = {{1, 1, 0}, {1, 1, 0}};
		glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(vec3), colors, GL_STATIC_DRAW);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	}

	void draw()
	{
		this->updateVertexCoords();

		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(vec3), this->vertexCoords, GL_DYNAMIC_DRAW);
		glDrawArrays(GL_LINES, 0, 2);
	}

	void updateVertexCoords()
	{
		this->vertexCoords[0] = vec3(this->node0->getCoordinate().x, this->node0->getCoordinate().y, this->node0->getCoordinate().z);
		this->vertexCoords[1] = vec3(this->node1->getCoordinate().x, this->node1->getCoordinate().y, this->node1->getCoordinate().z);
	}
};

class Graph
{
private:
	std::vector<Node *> nodes;
	std::vector<Edge *> edges;

public:
	void draw()
	{
		gpuPrograms[0].Use();
		for (auto edge : this->edges)
		{
			edge->draw();
		}
		gpuPrograms[1].Use();
		for (auto node : this->nodes)
		{
			node->draw();
		}
	}

	void addNode(Node *newNode)
	{
		for (auto node : this->nodes)
		{
			node->addNotAdjacentNode(newNode);
			newNode->addNotAdjacentNode(node);
		}
		this->nodes.push_back(newNode);
	}

	void addEdge(Edge *newEdge)
	{
		this->edges.push_back(newEdge);
	}

	void generateRandomNodes(int number)
	{
		for (int i = 0; i < number; ++i)
		{
			float LO = -1, HI = 1;
			// Itt ut?nan?ztem, hogy hogyan lehet random float-ot gener?lni. Forr?s: https://stackoverflow.com/questions/686353/random-float-number-generation
			float x = LO + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (HI - LO)));
			float y = LO + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (HI - LO)));
			float w = sqrtf(x * x + y * y + 1);
			this->addNode(new Node(vec3(x, y, w)));
		}
	}

	void genarateRandomEdges(int percentage)
	{
		for (int i = 0; i < (int)this->nodes.size(); ++i)
		{
			for (int j = i + 1; j < (int)this->nodes.size(); ++j)
			{
				if (rand() % 100 < percentage)
				{
					this->addEdge(new Edge(this->nodes[i], this->nodes[j]));
				}
			}
		}
	}

	void letItmove()
	{
		for (auto node : this->nodes)
		{
			node->letItmove();
		}
		glutPostRedisplay();
	}

	void translate(const vec3 &m1, const vec3 &m2)
	{
		for (auto node : this->nodes)
		{
			node->translate(m1, m2);
		}
		glutPostRedisplay();
	}

	void applyHeuristics(int n)
	{
		for (int i = 0; i < n; ++i)
		{
			for (auto node : this->nodes)
			{
				node->applyHeuristics();
			}
		}
		glutPostRedisplay();
	}

	~Graph()
	{
		for (auto node : this->nodes)
		{
			delete node;
		}
		for (auto edge : this->edges)
		{
			delete edge;
		}
	}
};

vec3 calculateMousePosition(int pX, int pY)
{
	float cX = 2.0f * pX / windowWidth - 1;
	float cY = 1.0f - 2.0f * pY / windowHeight;
	float w = sqrtf(-1 / (cX * cX + cY * cY - 1));
	return vec3(cX * w, cY * w, w);
}

bool notTooFar(vec3 vec)
{
	return vec.z < 2.5f;
}

Graph graph;

void onInitialization()
{
	graph.generateRandomNodes(50);
	graph.genarateRandomEdges(5);

	gpuPrograms[0].create(vertexSource0, fragmentSource0, "fragmentColor");
	gpuPrograms[1].create(vertexSource1, fragmentSource1, "fragmentColor");
}

void onDisplay()
{
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLineWidth(2);
	graph.draw();
	glutSwapBuffers();
}

void onKeyboard(unsigned char key, int pX, int pY)
{
	if (key == ' ')
	{
		graph.applyHeuristics(1);
		physicsEnabled = true;
	}
}

void onKeyboardUp(unsigned char key, int pX, int pY)
{
}

void onMouseMotion(int pX, int pY)
{
	if (mouseButtonDown && notTooFar(previousMousePosition))
	{
		vec3 currentMousePosition = calculateMousePosition(pX, pY);
		if (isNan(currentMousePosition))
		{
			return;
		}
		float d = distance(previousMousePosition, currentMousePosition);
		if (d > reallySmallFloat && notTooFar(currentMousePosition))
		{
			vec3 newMiddle = translatePoint(middle, previousMousePosition, currentMousePosition);
			if (notTooFar(newMiddle))
			{
				middle = newMiddle;
				graph.translate(previousMousePosition, currentMousePosition);
				previousMousePosition = currentMousePosition;
			}
		}
	}
}

void onMouse(int button, int state, int pX, int pY)
{
	if (button == GLUT_RIGHT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			mouseButtonDown = true;
			previousMousePosition = calculateMousePosition(pX, pY);
		}
		if (state == GLUT_UP)
		{
			mouseButtonDown = false;
		}
	}
}

inline void updateTime()
{
	long currentTime = glutGet(GLUT_ELAPSED_TIME);
	deltaTime = (float)(currentTime - previousTime) / 1000;
	previousTime = currentTime;
}

void onIdle()
{
	updateTime();
	if (physicsEnabled)
	{
		graph.letItmove();
	}
}
